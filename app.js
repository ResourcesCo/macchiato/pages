import { Layout } from "/components/layout.js"
import { Page } from "/components/page.js"
import { PageActions } from "/components/page-actions.js"
import { FileGroupPage } from "/components/file-group-page.js"
import { Header } from "/components/header.js"
import { NavMenu } from "/components/nav-menu.js"
import { Dialog } from "/dialog/dialog.js"
import { ButtonGroup } from "/forms/button-group.js"
import { Dropdown } from "/menu/dropdown.js"
import { PageSettings } from "/settings/page-settings.js"

customElements.define('m-layout', Layout)
customElements.define('m-page', Page)
customElements.define('m-page-actions', PageActions)
customElements.define(
  'm-settings-page-settings', PageSettings
)
customElements.define(
  'm-file-group-page', FileGroupPage
)
customElements.define('m-header', Header)
customElements.define('m-nav-menu', NavMenu)
customElements.define('m-dialog', Dialog)
customElements.define(
  'm-forms-button-group', ButtonGroup
)
customElements.define(
  'm-menu-dropdown', Dropdown
)

class Setup {
  async runWithSw() {
    navigator.serviceWorker.addEventListener(
      'controllerchange',
      () => {
        if (this.registration.active) {
          window.location.reload(true)
        }
      }
    )
    await this.register()
    this.load()
  }

  async runWithoutSw() {
    const layout = document.createElement(
      'm-layout'
    )
    layout.csp = undefined
    layout.header.menu.handleLinks = true
    document.body.appendChild(layout)
  }

  async register() {
    try {
      this.registration =
        await navigator.serviceWorker.register(
          '/sw.js',
          {scope: '/'}
        )
    } catch (err) {
      console.error(
        'error registering service worker', err
      )
    }
  }

  load() {
    if (this.registration.active) {
      document.body.appendChild(
        document.createElement('m-layout')
      )
    }
  }
}

new Setup().runWithSw()