const frameHtml = `<!doctype html>
<html>
  <head>
    <title>Frame</title>
    <style>
html, body, iframe {
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
}
iframe {
  border: none;
  display: block;
}
html {
  box-sizing: border-box;
}
*, *:before, *:after {
  box-sizing: inherit;
}
    </style>
  </head>
  <body>
    ${'<'}script type="module">
  let frame = undefined
addEventListener('message', event => {
  let isNew = false
  const d = event.data
  if (Array.isArray(d) && d[0] === 'srcdoc') {
    isNew = frame === undefined
    if (isNew) {
      frame = document.createElement('iframe')
      frame.sandbox = "allow-scripts allow-top-navigation"
    }
    frame.srcdoc = d[1]
  } else if (frame !== undefined) {
    frame.contentWindow.postMessage(event.data, '*')
  }
  if (isNew) {
    document.body.appendChild(frame)
  }
})
    ${'</'}script>
  </body>
</html>`

export class Page extends HTMLElement {
  constructor() {
    super()
    this.attachShadow({mode: 'open'})
    this.csp = "default-src 'self' 'unsafe-inline' 'unsafe-eval'"
    this.textArea = document.createElement('textarea')
    this.textArea.addEventListener('input', e => {
      this.body = e.target.value
    })
    const div = document.createElement('div')
    div.classList.add('twrap')
    div.appendChild(this.textArea)
    this.shadowRoot.appendChild(div)
    this.isGroup = false
  }

  connectedCallback() {
    this.textArea.value = this.body
    const style = document.createElement('style')
    style.textContent = `
      :host {
        display: grid;
        grid-template-columns: 1fr;
        grid-template-rows: 1fr;
        grid-template-areas: "main";
        flex-direction: column;
        align-items: stretch;
      }
      div.twrap {
        padding: 10px;
        display: flex;
        align-items: stretch;
        flex-direction: column;
        grid-area: main;
      }
      textarea {
        padding: 5px 10px;
        font-size: 0.90em;
        height: 100%;
      }
      iframe {
        border: none;
        margin: 0;
        padding: 0;
        grid-area: main;
        width: 100%;
      }
      :host(.editing) iframe {
        display: none;
      }
      :host(.viewing) textarea {
        display: none;
      }
    `
    this.shadowRoot.append(style)
    this.initFrame()
    this.editing = this.editing
  }

  initFrame() {
    const frame = document.createElement('iframe')
    if (this.csp !== undefined) {
      frame.sandbox = "allow-same-origin allow-scripts allow-top-navigation"
      const url = new URL(
        '/-/frame', location.href
      )
      url.searchParams.set('csp', this.csp)
      url.searchParams.set('html', frameHtml)
      frame.src = url.href
    } else {
      frame.sandbox = "allow-scripts allow-top-navigation"
      frame.srcdoc = frameHtml
    }
    frame.addEventListener('load', () => {
      this.display(this.body)
    })
    this.frame = frame
    this.shadowRoot.append(frame)
    this.textArea.addEventListener('blur', e => {
      this.display(e.target.value)
    })
  }

  display(value) {
    let doc = value || ''
    if (!(/<\w/).test(doc.substring(0, 30))) {
      doc = `<pre style="white-space: pre-wrap; margin: 8px 12px; font-family: monospace;">` +
        doc.replace("<", "&lt;").replace(">", "&gt;") +
      `</pre>`
    }
    const msg = ['srcdoc', doc]
    this.frame.contentWindow.postMessage(msg, '*')
  }

  set body(value) {
    try {
      localStorage.setItem(this.path, value)
    } catch (err) {
      console.error(err)
    }
  }

  get body() {
    try {
      return localStorage.getItem(this.path)
    } catch (err) {
      console.error(err)
      return ''
    }
  }

  set editing(value) {
    this._editing = value
    if (this.shadowRoot.host) {
      const classes = this.shadowRoot.host.classList
      if (this.editing) {
        classes.add('editing')
        classes.remove('viewing')
        this.textArea.focus()
      } else {
        classes.add('viewing')
        classes.remove('editing')
      }
    }
  }

  get editing() {
    return this._editing
  }
}