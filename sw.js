async function initCache() {
  const cache = await caches.open('v1')
  await cache.addAll([
    '/app.js',
    '/components/file-group-page.js',
    '/components/header.js',
    '/components/layout.js',
    '/components/nav-menu.js',
    '/components/page.js',
    '/components/page-actions.js',
    '/dialog/dialog.js',
    '/editor/app.js',
    '/editor/file-group.js',
    '/editor/file-view.js',
    '/editor/text-edit.js',
    '/forms/button-group.js',
    '/index.html',
    '/loader/builder.js',
    '/loader/editor-build.js',
    '/menu/dropdown.js',
    '/settings/page-settings.js',
  ]) //7
}

self.addEventListener("install", event => {
  self.skipWaiting()
  event.waitUntil(initCache())
})

async function cacheFirst(request) {
  if (request.url.includes('/-/frame')) {
    const url = new URL(request.url)
    if (url.pathname === '/-/frame') {
      const html = url.searchParams.get('html')
      const csp = url.searchParams.get('csp')
      return new Response(html, {
        headers: {
          'Content-Type': 'text/html; charset=utf-8',
          'Content-Security-Policy': csp,
        }
      })
    }
  }
  const resp = await caches.match(request)
  if (resp) {
    return resp
  } else {
    return fetch(request)
  }
}

self.addEventListener('fetch', event => {
  event.respondWith(cacheFirst(event.request))
})

self.addEventListener('activate', event => {
  event.waitUntil(clients.claim())
})